## dotfiles
My dotfiles use [gnu stow](https://www.gnu.org/software/stow/manual/stow.html)
to store and link them into my home directory. It's expected that this repo will
be cloned inside the home directory. As an example, running `stow zsh` will
create a symlink one directory up that links to zsh/.zshrc.
