export SPACESHIP_PROMPT_ASYNC=true
ZSH_THEME="spaceship"
# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export EDITOR=nvim

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="aussiegeek"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode spaceship-vi-mode)

# User configuration
pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

# Add brew to path
eval "$(/opt/homebrew/bin/brew shellenv)"

pathadd /usr/local/bin
#pathadd /usr/bin
#pathadd /bin
#pathadd /usr/sbin
#pathadd /sbin
#pathadd /snap/bin
pathadd ${HOME}/workspace/ytermusic/target/release
pathadd ${HOME}/workspace/snowman/target/release

# Script taken from here
# https://github.com/OpenAPITools/openapi-generator#launcher-script
pathadd ${HOME}/bin/openapitools/

# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh
eval spaceship_vi_mode_enable
# Add vi-mode indicator after line_sep
#spaceship add --after line_sep vi_mode

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias v='nvim'
alias vf='v $(fzf)'
alias zconfig="v ~/.zshrc && source ~/.zshrc && tmux display-message 'Reloaded zshrc'"
alias gch='git branch --sort=-committerdate | fzf --header Checkout | xargs git checkout'
function vconfig() {
  pushd > /dev/null
  cd ~/.config/nvim
  v ~/.config/nvim/init.lua
  popd > /dev/null
}

# Docker Compose alias
alias dc="docker-compose"

# git aliases
alias gpo='git push origin $(git rev-parse --abbrev-ref HEAD)'
alias gpo+='git push origin +$(git rev-parse --abbrev-ref HEAD)'
alias gcan='git commit --amend --no-edit'

function changessh() {
  eval "$(ssh-agent -s)"
  if [ "$1" = "home" ]; then
    ssh-add ~/.ssh/id_rsa_patoconnor43
    git config --local user.email patoconnor43@gmail.com
    git config user.name "Pat O'Connor" --replace-all
    export GH_TOKEN="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"
    elif [ "$1" = "work" ]; then
    ssh-add ~/.ssh/id_rsa
    git config --local user.email patrick.oconnor@workiva.com
    git config user.name "Patrick O'Connor" --replace-all
    export GH_TOKEN="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"
  fi;
}

function gist() {
  tmpfile=$(mktemp create-gist.XXXXXX.md)
  printf "### filename: `date +"%Y%m%d_%H%M%S"`_\n### description: \n" >> $tmpfile
  nvim $tmpfile
  filename=$(sed -n 's/### filename: \(.*\)$/\1/p' $tmpfile)
  description=$(sed -n 's/### description: \(.*\)$/\1/p' $tmpfile)
  mv "$tmpfile" "$filename"
  gh gist create "$filename" --desc "$description" --web
  rm "$filename"
}

function jwt() {
  # This function can either be called with a `|` or with a single argument. If it
  # is used with a `|`, "$1" will be empty and we can pass stdin to the jq command
  # If an argument is used, that argument will be redirected into the jq command
  #
  # Pieces taken from https://gist.github.com/angelo-v/e0208a18d455e2e6ea3c40ad637aac53
  if [ "$1" = "" ]; then
    jq -R 'gsub("-";"+") | gsub("_";"/") | split(".") | select(length > 0) | .[1] | @base64d | fromjson'
  else
    jq -R 'gsub("-";"+") | gsub("_";"/") | split(".") | select(length > 0) | .[1] | @base64d | fromjson' <<< "$1"
  fi
}



#UTF 8
#export LC_ALL=en_US.UTF-8
#export LANG=en_US.UTF-8

# go
export GOPRIVATE="github.com/Workiva/*"
export GOPATH="$HOME/workspace/goworkspace"
pathadd $GOPATH/bin
eval "$(ax --completion-script-zsh)"

# dart
pathadd $HOME/.pub-cache/bin
alias ddev="pub run dart_dev"


#rust
pathadd $HOME/.cargo/bin

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -x "$(command -v zoxide)" ] && eval "$(zoxide init zsh)"

# Add local pip installs to path
pathadd ${HOME}/.local/bin

# ASDF version manager config
. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash
