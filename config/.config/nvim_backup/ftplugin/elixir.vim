setlocal formatprg=mix\ format\ -
augroup PatElixirAutoCommands
    autocmd! BufWritePre *.ex :norm mpgggqG<CR>
    autocmd! BufWritePost *.ex :norm 'p<CR>
    "autocmd! BufWritePre *.leex ::%!prettier --parser html
augroup END
