"packadd packer.nvim
"lua << EOF
"--vim._update_package_paths()
"--vim.cmd[[colorscheme archery]]
"print('updated path')
"local packer = require('packer')
"packer.init()
"packer.compile('~/.config/nvim/plugin/packer_load.vim')
"EOF



"" Show list of options while tab-completing
"set wildmenu
"set wildmode=longest,list,full
"
"autosave when lose focus
:au FocusLost * :wa

" Set to auto read when a file is changed from the outside
set autoread
set cmdheight=2
" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect
set cursorline
set expandtab
set foldmethod=marker
set grepprg=rg\ --vimgrep\ $*
set grepformat=%f:%l:%c:%m
set hidden
" highlight search
set hlsearch
" ignore case when searching
set ignorecase
set laststatus=2
" Don't redraw when exec'ing macros
set lazyredraw
set noshowmode
set number
set relativenumber
set shiftwidth=4
set shortmess+=c
" Show matching backets when hoving over
set showmatch
set signcolumn=yes
set splitbelow
set splitright
set statusline+=%F
set tabstop=4
set termguicolors
set updatetime=100

let mapleader=" "

nnoremap <Leader>o :FZF<CR>
nnoremap <Leader>w :bdelete<CR>
" list buffers
nnoremap <Leader>l :Buffers<CR>

" Use <space><space> to toggle to the last buffer
nnoremap <leader><leader> <c-^>

" autocommands
autocmd FileType markdown setlocal textwidth=80
autocmd FileType markdown setlocal formatoptions-=c
autocmd FileType markdown setlocal spell

let g:rustfmt_autosave = 1
let g:airline_theme='monochrome'
let g:python3_host_prog = $HOME . '/.config/nvim/nvim_virtualenv/bin/python'
let g:python_host_prog  = $HOME . '/.config/nvim/python2_nvim_virtualenv/bin/python'

" Border style (rounded / sharp / horizontal)
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8, 'highlight': 'Todo', 'border': 'sharp' } }

let g:diagnostic_enable_virtual_text = 1
let g:diagnostic_enable_underline = 1
