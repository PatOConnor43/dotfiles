--- Neodev setup {{{
local sumneko_root_path = vim.fn.stdpath('cache')..'/lspconfig/sumneko_lua/lua-language-server'
local sumneko_binary = sumneko_root_path..'/bin/lua-language-server'
local lua_capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)
local luadev = require("neodev").setup({
    library = {
      vimruntime = true, -- runtime path
      types = true, -- full signature, docs and completion of vim.api, vim.treesitter, vim.lsp and others
      plugins = true, -- installed opt or start plugins in packpath
      -- you can also specify the list of plugins to make available as a workspace library
      -- plugins = { "nvim-treesitter", "plenary.nvim", "telescope.nvim" },
    },
    lspconfig = true,
    pathStrict = true,
})
--- }}}

local nvim_lsp = require('lspconfig')
local util = require('lspconfig.util')
local lsp_signature = require('lsp_signature')
--local completion = require('completion')
local lsp_status = require('lsp-status')
local closing_labels = require('lsp_extensions.dart.closing_labels')
local outline = require('lsp_extensions.dart.outline')
local jdtls = require('jdtls')
local dap = require('dap')

local function trim(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local mapper = function(mode, key, result)
  vim.api.nvim_buf_set_keymap(0, mode, key, result, {noremap = true, silent = true})
end

local custom_attach = function(client)
  --completion.on_attach(client)
  --lsp_signature.on_attach({
  --        bind = true, -- This is mandatory, otherwise border config won't get registered.
  --        handler_opts = {
  --            border = "single"
  --        },
  --        extra_trigger_chars = {','},
  --    })

  mapper('n', '<c-]>', '<cmd>lua vim.lsp.buf.declaration()<CR>')
  mapper('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
  mapper('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>')
  mapper('n', 'gD', '<cmd>lua vim.lsp.buf.implementation()<CR>')
  mapper('n', '1gD', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
  mapper('n', 'gr', '<cmd>Telescope lsp_references<CR>')
  mapper('n', 'gR', '<cmd>lua vim.lsp.buf.rename()<CR>')

  mapper('n', '<leader>sl', '<cmd>lua vim.diagnostic.open_float()<CR>')
  mapper('n', '<leader><enter>', '<cmd>lua vim.lsp.buf.code_action()<CR>')
  mapper('n', '<leader>cl', '<cmd>lua vim.lsp.codelens.run()<CR>')

  mapper('n', '<leader>k', '<cmd>lua vim.diagnostic.goto_prev()<CR>')
  mapper('n', '<leader>j', '<cmd>lua vim.diagnostic.goto_next()<CR>')


  mapper('i', '<c-s>', '<cmd>lua vim.lsp.buf.signature_help()<CR>')

  -- Not sure if this is right
  --vim.cmd("setlocal omnifunc=v:lua.vim.lsp.omnifunc")
  vim.cmd("autocmd BufEnter,CursorHold,InsertLeave <buffer> lua vim.lsp.codelens.refresh()")
end


local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

--- Lua config {{{
nvim_lsp.sumneko_lua.setup({cmd = {sumneko_binary}, on_attach = custom_attach, capabilities = lua_capabilities})
-- }}}

-- Rust config {{{
nvim_lsp.rust_analyzer.setup({on_attach = custom_attach, capabilities = capabilities})
-- }}}

-- Python config {{{
nvim_lsp.pyright.setup({on_attach = custom_attach, capabilities = capabilities})
-- }}}


-- Dart config {{{
local dart_bin_folder = trim(vim.fn.system("asdf where dart"))
local snapshot_path = util.path.join(dart_bin_folder, "dart-sdk/bin/snapshots/analysis_server.dart.snapshot")

local dart_callbacks = {} --lsp_status.extensions.dartls.setup()
dart_callbacks['dart/textDocument/publishClosingLabels'] = closing_labels.get_callback({highlight = "Cursor", prefix = " >> "})
dart_callbacks['dart/textDocument/publishOutline'] = outline.get_callback()

local dart_attach = function(client)
  custom_attach(client)
  vim.api.nvim_command('augroup dart_lsp_aucmds')
  vim.api.nvim_command('augroup END')
end

local dart_capabilities = vim.lsp.protocol.make_client_capabilities()
dart_capabilities.textDocument.completion.completionItem.snippetSupport = true
dart_capabilities.textDocument.codeAction = {
  dynamicRegistration = false;
      codeActionLiteralSupport = {
          codeActionKind = {
              valueSet = {
                 "",
                 "quickfix",
                 "refactor",
                 "refactor.extract",
                 "refactor.inline",
                 "refactor.rewrite",
                 "source",
                 "source.organizeImports",
              };
          };
      };
}
nvim_lsp.dartls.setup({
  on_attach = dart_attach,
  cmd = { "dart", snapshot_path, "--lsp"};
  filetypes = {"dart"};
  root_dir = nvim_lsp.util.root_pattern("pubspec.yaml");
  init_options = {
    onlyAnalyzeProjectsWithOpenFiles = false,
    suggestFromUnimportedLibraries = true,
    closingLabels = true,
    outline = true,
  };
  handlers = dart_callbacks;
  capabilities = dart_capabilities;
})
-- }}}

-- Go config {{{
local go_custom_attach = function(client)
  require('go').setup({
      verbose = true,
      go = 'go',
      gofmt = '',
      test_runner = 'richgo',
      log_path = '/tmp/gonvim.log',
      gocoverage_sign = "█",
      sign_priority = 5, -- change to a higher number to override other signs
      icons = {breakpoint = '🔴', currentpos = '👉'},
      lsp_inlay_hints = {
        enable = true,
        -- Only show inlay hints for the current line
        only_current_line = false,
        -- Event which triggers a refersh of the inlay hints.
        -- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
        -- not that this may cause higher CPU usage.
        -- This option is only respected when only_current_line and
        -- autoSetHints both are true.
        only_current_line_autocmd = "CursorHold",
        -- whether to show variable name before type hints with the inlay hints or not
        -- default: false
        show_variable_name = true,
        -- prefix for parameter hints
        parameter_hints_prefix = " ",
        show_parameter_hints = true,
        -- prefix for all the other hints (type, chaining)
        other_hints_prefix = "=> ",
        -- whether to align to the lenght of the longest line in the file
        max_len_align = false,
        -- padding from the left if max_len_align is true
          max_len_align_padding = 1,
          -- whether to align to the extreme right or not
          right_align = false,
          -- padding from the right if right_align is true
            right_align_padding = 6,
            -- The color of the hints
            highlight = "Comment",
          },
        })
      custom_attach(client)
    end

nvim_lsp.gopls.setup({
    on_attach = go_custom_attach,
    capabilities = capabilities,
    settings = {
      gopls = {
        env = {
          GOFLAGS = "-tags=integration"
        },
      },
    },
    init_options = {
      codelenses = {
        ["run test"] = true,
        start_debugging = true,
        run_tests = true,
        test = true,
        gc_details = false,
        generate = true,
        tidy = true,
        upgrade_dependency = true,
        vendor = true,
      },
      hints = {
        assignVariableTypes = true,
        functionTypeParameters = true,
        parameterNames = true,
        rangeVariableTypes = true,
      }
    },
    --handlers = {
      --  ['textDocument/codeLens'] = function(err, a, result, client_id, bufnr)
        --      print(vim.inspect(err))
        --      print(vim.inspect(a))
        --      print(vim.inspect(result))
      --    end 
    --}
})
    -- }}}

-- tsserver config {{{
local tsserver_capabillities = vim.lsp.protocol.make_client_capabilities()
tsserver_capabillities.textDocument.codeAction = {
  dynamicRegistration = false;
      codeActionLiteralSupport = {
          codeActionKind = {
              valueSet = {
                "",
                "quickfix",
                "refactor",
                "refactor.extract",
                "refactor.inline",
                "refactor.rewrite",
                "source",
                "source.organizeImports",
                "tsserver.organizeImports",
              };
          };
      };
}
nvim_lsp.tsserver.setup{on_attach = custom_attach, capabilities = tsserver_capabillities}
-- }}}

-- Java config {{{
local java_attach = function(client)
  custom_attach(client)
  jdtls.setup_dap()
  vim.cmd[[command! -buffer JdtTestMethod lua require('jdtls').test_nearest_method()]]
  vim.cmd[[command! -buffer JdtTestClass lua require('jdtls').test_class()]]
  --require('dap.ext.vscode').load_launchjs()

  -- `code_action` is a superset of vim.lsp.buf.code_action and you'll be able to
-- use this mapping also with other language servers
--nnoremap <A-CR> <Cmd>lua require('jdtls').code_action()<CR>
--vnoremap <A-CR> <Esc><Cmd>lua require('jdtls').code_action(true)<CR>
--nnoremap <leader>r <Cmd>lua require('jdtls').code_action(false, 'refactor')<CR>
--
--nnoremap <A-o> <Cmd>lua require'jdtls'.organize_imports()<CR>
--nnoremap crv <Cmd>lua require('jdtls').extract_variable()<CR>
--vnoremap crv <Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>
--vnoremap crm <Esc><Cmd>lua require('jdtls').extract_method(true)<CR>

  require('jdtls.setup').add_commands()
end

local bundles = {
  '/home/patrickoconnor/Downloads/dg.jdt.ls.decompiler.fernflower-0.0.2-201802221740.jar',
  '/home/patrickoconnor/Downloads/dg.jdt.ls.decompiler.common-0.0.2-201802221740.jar',
  '/home/patrickoconnor/workspace/fernflower/build/libs/fernflower.jar',
  vim.fn.glob("/home/patrickoconnor/workspace/java-debug/com.microsoft.java.debug.plugin/target/com.microsoft.java.debug.plugin-*.jar")
}
vim.list_extend(bundles, vim.split(vim.fn.glob("/home/patrickoconnor/workspace/vscode-java-test/server/*.jar"), "\n"))
JAVA_CONFIG = {
  name = "jdtls",
  on_attach = java_attach,
  cmd = {"java-lsp"},
  filetypes = {"java"},
  init_options = {
    --workspace = util.path.join { vim.loop.os_homedir(), "workspace/eclipse_workspace" },
    --vm_args = {'-javaagent:/home/patrickoconnor/Downloads/lombok.jar'},
    bundles = bundles,
  extendedClientCapabilities = jdtls.extendedClientCapabilities,
  settings = {
    java = {
      contentProvider = {
        preferred = 'fernflower',
      },
    },
  },
  },
}

--jdtls.start_or_attach(java_config)
vim.cmd [[augroup javalsp]]
vim.cmd [[au!]]
vim.cmd [[au FileType java lua require('jdtls').start_or_attach(JAVA_CONFIG)]]
vim.cmd [[augroup end]]
-- }}}

nvim_lsp.bashls.setup({on_attach = custom_attach})

nvim_lsp.elixirls.setup({on_attach = custom_attach, cmd = {'/home/patrickoconnor/workspace/elixir/elixirls/language_server.sh'}, capabilities = capabilities})

nvim_lsp.cssls.setup({on_attach = custom_attach, capabilities = capabilities})

--nvim_lsp.pylsp.setup({on_attach = custom_attach, capabilities = capabilities})

nvim_lsp.jsonls.setup({on_attach = custom_attach, settings = {
      json = {
        schemas = {
        {
          url = "https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json",
          fileMatch = {"**/resume.json", "*/resume.json", "*resume.json", "resume.json"},
          name = "Resume JSON",
          versions = {
            ["1.0.0"] = "https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json"
          }
        },
        }
      }
  }})

nvim_lsp.kotlin_language_server.setup({on_attach = custom_attach})

nvim_lsp.yamlls.setup({on_attach = custom_attach, settings = {
      yaml = {
        schemas = {
  ["https://raw.githubusercontent.com/OAI/OpenAPI-Specification/master/schemas/v3.0/schema.json"] = "openapi.yaml",
  ["https://json.schemastore.org/pubspec"] = "pubspec.yaml",
  ["https://json.schemastore.org/dart-build"] = "build.yaml",
  ["https://json.schemastore.org/dart-test"] = "dart_test.yaml",
  ["https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json"] = "resume.yaml"
}}

  }})
nvim_lsp.gdscript.setup({on_attach = custom_attach})

nvim_lsp.kotlin_language_server.setup({on_attach = custom_attach, capabilities = capabilities, cmd = {util.path.join { vim.loop.os_homedir(), 'workspace/kotlin-language-server/server/build/install/server/bin/kotlin-language-server'}}})


vim.api.nvim_create_autocmd({'UIEnter'}, {
  once = true,
  callback = function()
    local Spinner = require('spinner')
    local spinners = {}

    local function format_msg(msg, percentage)
      msg = msg or ''
      if not percentage then
        return msg
      end
      return string.format('%2d%%\t%s', percentage, msg)
    end

    vim.api.nvim_create_autocmd({'User'}, {
      pattern = {'LspProgressUpdate'},
      group = vim.api.nvim_create_augroup('LSPNotify', {clear = true}),
      desc = 'LSP progress notifications',
      callback = function()
        for _, c in ipairs(vim.lsp.get_active_clients()) do
          for token, ctx in pairs(c.messages.progress) do
            if not spinners[c.id] then
              spinners[c.id] = {}
            end
            local s = spinners[c.id][token]
            if not ctx.done then
              if not s then
                spinners[c.id][token] = Spinner(
                  format_msg(ctx.message, ctx.percentage), vim.log.levels.INFO, {
                    title = ctx.title and string.format('%s: %s', c.name, ctx.title) or c.name
                  })
              else
                s:update(format_msg(ctx.message, ctx.percentage))
              end
            else
              c.messages.progress[token] = nil
              if s then
                s:done(ctx.message or 'Complete', nil, {
                  icon = '',
                })
                spinners[c.id][token] = nil
              end
            end
          end
        end
      end,
    })
  end,
})


