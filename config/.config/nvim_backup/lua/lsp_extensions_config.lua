--vim.cmd [[augroup DartShowClosingLabels]]
--vim.cmd [[  au!]]
--vim.cmd [[  autocmd CursorHold,CursorHoldI *.dart :lua require('lsp_extensions.dart.closing_labels').draw_labels()]]
--vim.cmd [[augroup END]]
--vim.cmd [[autocmd DartShowClosingLabels CursorHold,CursorHoldI *.dart :lua require('lsp_extensions.dart.closing_labels').draw_labels()]]

DART_KIND_PREFIXES = {
        CLASS = "",
        CLASS_TYPE_ALIAS = "",
        COMPILATION_UNIT = "ﴒ",
        CONSTRUCTOR = "",
        CONSTRUCTOR_INVOCATION = "",
        ENUM = "טּ",
        ENUM_CONSTANT = "יּ",
        EXTENSION = "",
        FIELD = "ﬧ",
        FILE = "",
        FUNCTION = "",
        FUNCTION_INVOCATION = "",
        FUNCTION_TYPE_ALIAS = "",
        GETTER = "",
        LABEL = "",
        LIBRARY = "",
        LOCAL_VARIABLE = "",
        METHOD = "",
        MIXIN = "ﭚ",
        PARAMETER = "",
        PREFIX = "並",
        SETTER = "",
        TOP_LEVEL_VARIABLE = "ﬢ",
        TYPE_PARAMETER = "",
        UNIT_TEST_GROUP = "﬽",
        UNIT_TEST_TEST = "",
        UNKNOWN = "",
}

DART_SHOW_OUTLINE = function()
    require('lsp_extensions.dart.outline').telescope({kind_prefixes=DART_KIND_PREFIXES})
end

local inlay_hints = require('lsp_extensions.inlay_hints')
ShowInlineInlayHints = function()
  vim.lsp.buf_request(0, 'rust-analyzer/inlayHints', inlay_hints.get_params(), inlay_hints.get_callback {})
end

