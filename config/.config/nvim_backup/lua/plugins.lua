-- Only required if you have packer in your `opt` pack
vim.cmd [[packadd packer.nvim]]

local packer_exists = pcall(require, 'packer')

if not packer_exists then
    local directory = string.format(
        '%s/site/pack/packer/opt', vim.fn.stdpath('data'))

    vim.fn.mkdir(directory, 'p')

    local out = vim.fn.system(
        string.format(
            'git clone %s %s',
            'https://github.com/wbthomason/packer.nvim',
            directory .. '/packer.nvim'
        )
    )

    print(out)
    print("Downloading packer.nvim...")

    return
end


return require('packer').startup(function()
    local use = require('packer').use

    use {'wbthomason/packer.nvim', opt = true}

    -- Colorschemes {{{
    use {'gilgigilgil/anderson.vim'}
    use {'Badacadabra/vim-archery'}
    use {'sainnhe/vim-color-forest-night'}
    use {'beikome/cosme.vim'}
    use {'junegunn/seoul256.vim'}
    use {'sainnhe/gruvbox-material'}
    use {'flrnd/candid.vim'}
    use {'liuchengxu/space-vim-dark'}
    use {'sansbrina/vim-poolside', branch = 'release'}
    use {'ghifarit53/daycula-vim', branch = 'main'}
    use {'folke/tokyonight.nvim'}
    -- }}}

    -- Editor {{{
    use {'nvim-treesitter/playground', config = function()

    end}
    use {'rcarriga/nvim-notify', config = function ()
        local notify = require('notify')
        notify.setup({
            on_open = function(win)
                if vim.api.nvim_win_is_valid(win) then
                    vim.api.nvim_win_set_config(win, { border = 'single'})
                    end
                end,
        })
        vim.notify = notify
        end
    }
    use {
        'glepnir/galaxyline.nvim',
        branch = 'main',
        -- your statusline
        config = function() require('statusline_config') end,
        -- some optional icons
        requires = {'kyazdani42/nvim-web-devicons', opt = true}
    }
    use {
        'folke/trouble.nvim',
        config = function()
            require("trouble").setup {
                -- your configuration comes here
                -- or leave it empty to use the default settings
                -- refer to the configuration section below
            }
        end
    }
    use {'tpope/vim-fugitive'}
    use {'habamax/vim-godot'}
    use {'preservim/vimux'}
    use {'vim-test/vim-test'}
    use {'weirongxu/plantuml-previewer.vim'}
    use {'tyru/open-browser.vim'}
    use {'mhinz/vim-startify'}
    use {'mhinz/vim-signify'}
    use {'honza/vim-snippets'}
    use {'hrsh7th/vim-vsnip'}
    use {'mattn/emmet-vim'}
    use {'hrsh7th/vim-vsnip-integ'}
    use {'sbdchd/neoformat'}

    use {'vimwiki/vimwiki'}
    use {'pwntester/octo.nvim', requires = {
            'nvim-lua/telescope.nvim',
            'kyazdani42/nvim-web-devicons',
        }
    }
    use {'pgr0ss/vim-github-url'}
    use {'PatOConnor43/sourcegraph-vim'}
    use {'tpope/vim-surround'}
    use {'rhysd/git-messenger.vim'}
    use {
        'iamcco/markdown-preview.nvim',
        run = 'cd app && yarn install',
    }
    --use {'scrooloose/vim-slumlord', opt = true, ft = {'uml', 'puml', 'plantuml'}}
    use {'junegunn/vim-easy-align', ft = 'md'}

    use {'junegunn/fzf', run = './install --all' }
    use {'junegunn/fzf.vim'}
    use {'sheerun/vim-polyglot'}
    use {'nvim-lua/plenary.nvim'}
    --use {'https://github.com/glacambre/firenvim'}
    use {'nvim-lua/telescope.nvim', requires = {'nvim-lua/popup.nvim'}}
    use {'kyazdani42/nvim-web-devicons'}
    use {'bfredl/nvim-luadev'}
    use {'wakatime/vim-wakatime'}
    use {'johngrib/vim-game-code-break'}

    --use {'epwalsh/obsidian.nvim', config = function()
    --    require("obsidian").setup({
    --            dir = "~/obsidiannotes/Notes",
    --            completion = {
    --                nvim_cmp = true, -- if using nvim-cmp, otherwise set to false
    --                },
    --                note_id_func = function(title)
    --                    -- Create note IDs in a Zettelkasten format with a timestamp and a suffix.
    --                    local suffix = ""
    --                    if title ~= nil then
    --                        -- If title is given, transform it into valid file name.
    --                        suffix = title:gsub(" ", "-"):gsub("[^A-Za-z0-9-]", ""):lower()
    --                    else
    --                        -- If title is nil, just add 4 random uppercase letters to the suffix.
    --                        for _ in 1, 4 do
    --                            suffix = suffix .. string.char(math.random(65, 90))
    --                        end
    --                    end
    --                    return tostring(os.time()) .. "-" .. suffix
    --                end
    --            })
    --        end
    --    }

    use {'~/workspace/workiva.nvim',
      config = function() 
          local w = require('workiva')
          w.setup({
                  debug = true,
                  credentials = {
                      client_id = "8d5d4444d9a444c8bf1afb90d840fb7f",
                      secret = "6dff7da0b58bcfc40ac5152ac16ec77209a11aa8f1f840be",
                      iam_domain = "https://api.wk-dev.wdesk.org/iam/v1",
                      platform_domain = "https://api.wk-dev.wdesk.org/platform/v1",
                      content_domain = "https://h.wk-dev.wdesk.org/s/cerberus/content-api/v0",
                  },
          })
      end
  }

    --}}}

    -- Language Support {{{
    use {'dart-lang/dart-vim-plugin', opt = true, ft = 'dart'}
    use {'rust-lang/rust.vim', opt = true, ft = {'rust'}}
    --use {'fatih/vim-go', run = ':GoInstallBinaries'}
    use {'ray-x/go.nvim', requires = {'ray-x/guihua.lua'}}
    use {'elixir-editors/vim-elixir', opt = true, ft = {'ex', 'exs'}}
    use {'folke/neodev.nvim'}
    use {'udalov/kotlin-vim', opt = true, ft = {'kts', 'kt'}}

    --}}}

    -- LSP {{{
    use {
        'neovim/nvim-lspconfig',
        requires = {
            --{'haorenW1025/completion-nvim'},
            --{'hrsh7th/nvim-compe'},
            {'hrsh7th/nvim-cmp',
                requires = {
                    'hrsh7th/cmp-buffer',
                    'hrsh7th/cmp-nvim-lsp',
                    'hrsh7th/cmp-vsnip',
                    'hrsh7th/cmp-nvim-lua',
                    'hrsh7th/cmp-emoji',
                    'onsails/lspkind-nvim',
                    --'~/workspace/cmp-pubspec',
                }
            },
            {'ray-x/lsp_signature.nvim'},
            --{'nvim-lua/diagnostic-nvim'},
            {'nvim-treesitter/nvim-treesitter'},
            {'nvim-treesitter/playground'},
            --{'tjdevries/lsp_extensions.nvim.git'},
            {'~/workspace/lsp-status.nvim'},
            {'~/workspace/lsp_extensions.nvim'},
            {'simrat39/rust-tools.nvim', config = function ()
                    require('rust-tools').setup({})
            end},
            {'mfussenegger/nvim-jdtls'},
            {'mfussenegger/nvim-dap', requires = {'rcarriga/nvim-dap-ui'}},
            {'nvim-telescope/telescope-dap.nvim'},
            {'theHamsta/nvim-dap-virtual-text'},
        }
    }
    --}}}

end)
