-- Setup nvim-cmp.
local cmp = require'cmp'
local lspkind = require'lspkind'

cmp.setup({
    snippet = {
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
        -- require'snippy'.expand_snippet(args.body) -- For `snippy` users.
      end,
    },
    --mapping = {
    --  ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    --  ['<C-f>'] = cmp.mapping.scroll_docs(4),
    --  ['<C-Space>'] = cmp.mapping.complete(),
    --  ['<C-e>'] = cmp.mapping.close(),
    --  ['<C-y>'] = cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Replace }),
    --},
    mapping = cmp.mapping.preset.insert({
      ['<C-d>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.close(),
      ['<C-y>'] = cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Replace }),
    }),
    sources = cmp.config.sources({
        { name = 'nvim_lsp'},
        { name = 'pub' },
        { name = 'emoji' },
        { name = 'nvim_lua' },
        { name = 'buffer' },
        { name = 'vsnip' }, -- For vsnip users.
      }),
    experimental = {
      ghost_text = true,
    },
    view = {
      entries = 'native'
    },
    formatting = {
      format = lspkind.cmp_format({with_text = true, maxwidth = 50,
          menu = ({
              nvim_lsp = "[LSP]",
              pub = "[Pub]",
              emoji = "[Emoji]",
              nvim_lua = "[Lua]",
              buffer = "[Buffer]",
              vsnip = "[VSnip]",
            })
        })
    }
})

