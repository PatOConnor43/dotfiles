--vim.cmd[[colorscheme daycula]]
require('plugins')
require('dap_config')
require('lsp_config')
require('lsp_extensions_config')
require('tree_sitter_config')
require('completion_config')

OPENAPI_SHOW_PATHS = function()
  local job = require('plenary.job')
  local j = job:new {
    command = "/home/patrickoconnor/workspace/openapi-schemer/target/release/openapi-schemer",
    args = {"apis/prototype-admin.yaml", "path", "list", "--vimgrep"},
  }
  local result, code = j:sync()
  if code ~= 0 then
    print("FAILED " .. code)
    return
  end

  local entries = {}
  for i, v in ipairs(result) do
    -- split the string and store the results in a new table
    local parts = {}
    for part in string.gmatch(v, "[^:]+") do
      table.insert(parts, part)
    end
    table.insert(entries, {filename = parts[1], lnum = parts[2], col = parts[3], text = parts[4]})
  end

  print(vim.inspect(entries))
  vim.fn.setqflist(entries, "r")
end


--require'compe'.setup {
--  enabled = true;
--  autocomplete = true;
--  debug = false;
--  min_length = 1;
--  preselect = 'enable';
--  throttle_time = 80;
--  source_timeout = 200;
--  resolve_timeout = 800;
--  incomplete_delay = 400;
--  max_abbr_width = 100;
--  max_kind_width = 100;
--  max_menu_width = 100;
--  documentation = true;
--
--  source = {
--    path = true;
--    buffer = true;
--    calc = true;
--    nvim_lsp = true;
--    nvim_lua = true;
--    vsnip = true;
--    ultisnips = true;
--  };
--}

--vim.cmd [[highlight link LspDiagnosticsError Exception]]
--vim.cmd [[highlight link LspDiagnosticsHint Cursor]]
--vim.cmd [[highlight link LspDiagnosticsInformation Type]]
--vim.cmd [[highlight link LspDiagnosticsWarning Special]]
