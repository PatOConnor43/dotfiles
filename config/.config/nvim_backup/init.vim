"" Show list of options while tab-completing
"set wildmenu
"set wildmode=longest,list,full
"
"autosave when lose focus
:au FocusLost * :wa

" Set to auto read when a file is changed from the outside
set autoread
set cmdheight=1
set cursorline
set expandtab
set foldmethod=marker
set grepprg=rg\ --vimgrep\ $*
set grepformat=%f:%l:%c:%m
set hidden
" highlight search
set hlsearch
" ignore case when searching
set ignorecase
set laststatus=3
" Don't redraw when exec'ing macros
set lazyredraw
set noshowmode
set number
set relativenumber
set shiftwidth=4
" Show matching backets when hoving over
set showmatch
set signcolumn=yes
set splitbelow
set splitright
set statusline+=%F
set tabstop=4
set termguicolors
set updatetime=100
colo tokyonight

let mapleader=" "

nnoremap <Leader>o :Telescope git_files<CR>
nnoremap <Leader>O :Telescope find_files<CR>
nnoremap <Leader>q :bdelete<CR>
" list buffers
nnoremap <Leader>l :Telescope buffers<CR>

" git-messenger.vim
nnoremap <Leader>mm :GitMessenger<CR>

" Use <space><space> to toggle to the last buffer
nnoremap <leader><leader> <c-^>

function! LspOpenLogCommand()
lua << EOF
  local log = vim.lsp.get_log_path()
  vim.cmd('view ' .. log)
  vim.cmd('norm! G')
  vim.cmd('augroup LspOpenLogCommandGroup')
  vim.cmd('  autocmd! CursorHold <buffer> view')
  vim.cmd('augroup END')
EOF
endfunction

command! LspClients :lua print(vim.inspect(vim.lsp.get_active_clients()))
command! LspStop :lua vim.lsp.stop_client(vim.lsp.get_active_clients())
command! LspOpenLog :call LspOpenLogCommand()

function! DFormat()
    " We have to write the buffer so the tools can work on the saved files
    write
    " run the formatter
    silent !pub run over_react_format --line-length=120 %:p
    " Re-load the buffer
    edit
endfunction

"nnoremap <silent> <leader>f :call DFormat()<CR>

" autocommands
autocmd FileType markdown setlocal textwidth=80
autocmd FileType markdown setlocal formatoptions+=a
autocmd FileType markdown setlocal formatoptions-=c
autocmd FileType markdown setlocal spell
autocmd FileType markdown setlocal paste
autocmd FileType vimwiki setlocal textwidth=80
autocmd FileType vimwiki setlocal formatoptions+=a
autocmd FileType vimwiki setlocal formatoptions-=c
autocmd FileType vimwiki setlocal spell
autocmd FileType vimwiki setlocal paste

augroup PatAutoCommands
    "autocmd! BufWrite *.dart call DFormat()
    "autocmd! LspDiagnosticsUpdated,LspDiagnosticsChanged,LspStatusUpdate *.dart LspStatus()
    autocmd! BufRead,BufNewFile *.frugal set filetype=thrift
augroup END

" VimWiki
command! Diary VimwikiDiaryIndex
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md',
                      \ 'auto_toc': 1,
                      \ 'auto_tags': 1,
                      \ 'auto_generate_tags': 1,
                      \ 'auto_generate_links': 1,
                      \ 'auto_diary_index': 1,
                      \ }]



let g:rustfmt_autosave = 1
let g:airline_theme='monochrome'
let g:python3_host_prog = $HOME . '/.config/nvim/nvim_virtualenv/bin/python'
let g:python_host_prog  = $HOME . '/.config/nvim/python2_nvim_virtualenv/bin/python'

"let g:go_gopls_enabled = 0

" Emmet
let g:user_emmet_leader_key='<C-i>'


" Border style (rounded / sharp / horizontal)
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8, 'highlight': 'Todo', 'border': 'sharp' } }

"let g:diagnostic_enable_virtual_text = 1
"let g:diagnostic_enable_underline = 0
"let g:diagnostic_insert_delay = 1
"let g:diagnostic_virtual_text_prefix = '#'

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect
set shortmess+=c
highlight link CompeDocumentation NormalFloat

" Snippets
imap <expr> <C-j>   vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-j>'
imap <expr> <C-k>   vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<C-k>'

"echo luaeval('PatTestFunction')
"echo type(luaeval('PatTestFunction'))
"echo type({ -> 5 })

