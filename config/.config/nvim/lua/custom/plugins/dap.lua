local tsquery = require("vim.treesitter.query")
local treesitter = require("vim.treesitter")

local tests_query = [[
(function_declaration
  name: (identifier) @testname
  parameters: (parameter_list
    . (parameter_declaration
      type: (pointer_type) @type) .)
  (#match? @type "*testing.(T|M)")
  (#match? @testname "^Test.+$")) @parent
]]

local subtests_query = [[
(call_expression
  function: (selector_expression
    operand: (identifier)
    field: (field_identifier) @run)
  arguments: (argument_list
    (interpreted_string_literal) @testname
    (func_literal))
  (#eq? @run "Run")) @parent
]]

local mapper = function(mode, key, result, desc)
  vim.api.nvim_set_keymap(mode, key, result, {noremap = true, silent = true, desc = desc})
end

local function show_configurations(opts)
    require('dap.ext.vscode').load_launchjs()
    require('telescope').load_extension('dap')
    require('telescope').extensions.dap.configurations()
end

local function get_closest_above_cursor(test_tree)
  local result
  for _, curr in pairs(test_tree) do
    if not result then
      result = curr
    else
      local node_row1, _, _, _ = curr.node:range()
      local result_row1, _, _, _ = result.node:range()
      if node_row1 > result_row1 then
        result = curr
      end
    end
  end
  if result.parent then
    return string.format("%s/%s", result.parent, result.name)
  else
    return result.name
  end
end

local function is_parent(dest, source)
  if not (dest and source) then
    return false
  end
  if dest == source then
    return false
  end

  local current = source
  while current ~= nil do
    if current == dest then
      return true
    end

    current = current:parent()
  end

  return false
end

local function get_closest_test()
  local stop_row = vim.api.nvim_win_get_cursor(0)[1]
  local ft = vim.api.nvim_buf_get_option(0, 'filetype')
  assert(ft == 'go', 'dap-go error: can only debug go files, not '..ft)
  local parser = vim.treesitter.get_parser(0)
  local root = (parser:parse()[1]):root()

  local test_tree = {}

  local test_query = tsquery.parse(ft, tests_query)
  assert(test_query, 'dap-go error: could not parse test query')
  for _, match, _ in test_query:iter_matches(root, 0, 0, stop_row) do
    local test_match = {}
    for id, node in pairs(match) do
      local capture = test_query.captures[id]
      if capture == "testname" then
        local name = treesitter.get_node_text(node, 0)
        test_match.name = name
      end
      if capture == "parent" then
        test_match.node = node
      end
    end
    table.insert(test_tree, test_match)
  end

  local subtest_query = tsquery.parse(ft, subtests_query)
  assert(subtest_query, 'dap-go error: could not parse test query')
  for _, match, _ in subtest_query:iter_matches(root, 0, 0, stop_row) do
    local test_match = {}
    for id, node in pairs(match) do
      local capture = subtest_query.captures[id]
      if capture == "testname" then
        local name = treesitter.get_node_text(node, 0)
        test_match.name = string.gsub(string.gsub(name, ' ', '_'), '"', '')
      end
      if capture == "parent" then
        test_match.node = node
      end
    end
    table.insert(test_tree, test_match)
  end

  table.sort(test_tree, function(a, b)
    return is_parent(a.node, b.node)
  end)

  for _, parent in ipairs(test_tree) do
    for _, child in ipairs(test_tree) do
      if is_parent(parent.node, child.node) then
        child.parent = parent.name
      end
    end
  end

  return get_closest_above_cursor(test_tree)
end

local function run_test(testname)
    local dap = require('dap')
  dap.run({
      type = "go",
      name = testname,
      request = "launch",
      mode = "test",
      program = "./${relativeFileDirname}",
      args = {"-test.run", testname},
    })
end


local function debug_test(opts)
  local testname = get_closest_test()
  vim.notify(string.format("starting debug session '%s'...", testname))
  run_test(testname)
end



return {
    'mfussenegger/nvim-dap',
    dependencies = {
        'nvim-telescope/telescope-dap.nvim',
        'rcarriga/nvim-dap-ui',
        'theHamsta/nvim-dap-virtual-text',
        'nvim-telescope/telescope-dap.nvim'
    },
    config = function ()
        local dap = require('dap')
        local dapui = require("dapui")
        dapui.setup()
        --- Settings {{{
            vim.fn.sign_define('DapBreakpoint', {text='📌', texthl='', linehl='', numhl=''})
            vim.fn.sign_define('DapStopped', {text='⮕ ', texthl='', linehl='', numhl=''})
            --vim.cmd('autocmd BufWritePost ~/.config/nvim/lua/dap_config.lua :luafile %')
        --- }}}

        --- KeyBindings {{{
            mapper('n', '<leader>dh', ':lua require\'dap\'.toggle_breakpoint()<CR>')
            mapper('n', '<leader><S-k>', ':lua require\'dap\'.step_out()<CR>')
            mapper('n', '<leader><S-l>', ':lua require\'dap\'.step_into()<CR>')
            mapper('n', '<leader><S-j>', ':lua require\'dap\'.step_over()<CR>')
            mapper('n', '<leader>ds', ':lua require\'dap\'.terminate()<CR>')
            mapper('n', '<leader>dn', ':lua require\'dap\'.continue()<CR>')
            mapper('n', '<leader>dk', ':lua require\'dap\'.up()<CR>')
            mapper('n', '<leader>dj', ':lua require\'dap\'.down()<CR>')
            mapper('n', '<leader>d_', ':lua require\'dap\'.run_last()<CR>')
            mapper('n', '<leader>dr', ':lua require\'dap\'.repl.open({}, \'vsplit\')<CR><C-w>l')
            mapper('n', '<leader>di', ':lua require\'dapui\'.eval()<CR>')
            mapper('v', '<leader>di', ':lua require\'dapui\'.eval()<CR>')
            mapper('n', '<leader>de', ':lua require\'dapui\'.float_element(\'watches\', {enter = true, width = 100})<CR>')

            mapper('n', '<leader>d?', ':lua require\'dap\'.ui.variables.scopes()<CR>')
            --mapper('n', '<leader>de', ':lua require\'dap\'.set_exception_breakpoints({"all"})<CR>')
            --mapper('n', '<leader>da', ':lua require\'debugHelper\'.attach()<CR>')
            --mapper('n', '<leader>dA', ':lua require\'debugHelper\'.attachToRemote()<CR>')

            --mapper('n', '<leader>di', ':lua require\'dap.ui.widgets\'.hover()<CR>')
            --mapper('n', '<leader>d?', ':lua local widgets=require\'dap.ui.widgets\';widgets.centered_float(widgets.scopes)<CR>')
            vim.api.nvim_create_user_command('DAPShowConfigurations', show_configurations, {})
            mapper('n', '<leader>dd', ':DAPShowConfigurations<CR>', 'DAP: Show Configurations')
            vim.api.nvim_create_user_command('DAPDebugNearestTest', debug_test, {})
            mapper('n', '<leader>dt', ':DAPDebugNearestTest<CR>', 'DAP: Debug Nearest Test')
        --- }}}

        --- Telescope {{{
            require('telescope').setup({})
            require('telescope').load_extension('dap')
            mapper('n', '<leader>df', ':Telescope dap frames<CR>')
            mapper('n', '<leader>dc', ':Telescope dap commands<CR>')
            mapper('n', '<leader>db', ':Telescope dap list_breakpoints<CR>')
        --- }}}

        --- Dart (doesn't seem to work) {{{
        --dap.adapters.dart = {
        --  type = "executable",
        --  command = "node",
        --  args = {"~/workspace/Dart-Code/out/dist/debug.js", "dart"}
        --}
        --dap.configurations.dart = {
        --  {
        --        type = "dart",
        --        request = "launch",
        --        name = "Launch dart",
        --        dartSdkPath = trim(vim.fn.system("asdf where dart").."/dart-sdk"),
        --        flutterSdkPath = trim(vim.fn.system("asdf where flutter")),
        --        program = "${workspaceFolder}/server/bin.dart",
        --        cwd = "${workspaceFolder}",
        --      },
        --  {
        --        type = "dart",
        --        request = "launch",
        --        name = "Launch flutter",
        --        dartSdkPath = trim(vim.fn.system("asdf where dart").."/dart-sdk"),
        --        flutterSdkPath = trim(vim.fn.system("asdf where flutter")),
        --        program = "${workspaceFolder}/lib/main.dart",
        --        cwd = "${workspaceFolder}",
        --      },
        --      {
        --        type = "dart",
        --        request = "attach",
        --        name = "Connect flutter",
        --        dartSdkPath = trim(vim.fn.system("asdf where dart").."/dart-sdk"),
        --        flutterSdkPath = trim(vim.fn.system("asdf where flutter")),
        --        program = "${workspaceFolder}/lib/main.dart",
        --        cwd = "${workspaceFolder}",
        --      },
        --}
        ---}}}

    -- Go {{{
    dap.adapters.go = function(callback, config)
      local handle
      --local pid_or_err
      local port = 38697
      handle, _ = vim.loop.spawn(
        "dlv",
        {
          args = {"dap", "-l", "127.0.0.1:" .. port},
          detached = true
        },
        function(code)
            if handle then
                handle:close()
            end
          print("Delve exited with exit code: " .. code)
        end
        )
      -- Wait 100ms for delve to start
      vim.defer_fn(
        function()
          --dap.repl.open()
          callback({type = "server", host = "127.0.0.1", port = port})
        end,
        100)
      --callback({type = "server", host = "127.0.0.1", port = port})
    end
    --
      -- https://github.com/go-delve/delve/blob/master/Documentation/usage/dlv_dap.md
    --  dap.configurations.go = {
    --    {
    --      type = "go",
    --      name = "Debug endpoints-admin (staging)",
    --      request = "launch",
    --      program = "${workspaceFolder}/main.go"
    --    },
    --    {
    --      name = "Launch staging server",
    --      type = "go",
    --      request = "launch",
    --      mode = "debug",
    --      program = "${workspaceFolder}/cmd/endpoints/main.go",
    --      env = {
    --        WORKIVA_CLUSTER_NAME = "staging",
    --        IAM_HOST = "https://staging.wdesk.org",
    --        IAM_CLIENT_ID = "endpoints-service",
    --        IAM_PRIVATE_KEY = "./keys/endpoints-service-staging.pem",
    --        USE_CLIENT_CREDS = "true"
    --      },
    --      args = {},
    --      cwd = "${workspaceFolder}"
    --    },
    --
    --    {
    --      type = "go",
    --      name = "Debug test", -- configuration for debugging test files
    --      request = "launch",
    --      mode = "test",
    --      program = "${fileDirname}/...",
    --    },
    --}
    -- }}}

    dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.open()
    end
    dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
    end
    dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close()
    end
end


}

